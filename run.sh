#!/bin/bash
logger "gitlab CI: running" -- $@
source $(dirname "$0")/base.sh

systemd-nspawn --quiet -D "$ROOTFS" \
  --overlay="$ROOTFS:$OVERLAY:/" \
  --bind-ro="$TMPDIR" \
  --machine=$MACHINE "$@"

if [ $? -ne 0 ]; then
    # mark the build as failure in GitLab CI
    exit $BUILD_FAILURE_EXIT_CODE
fi
